class Arbol {

    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
        this.nivel =prompt("Elija el nivel que desea buscar");
        this.busquedaElemento;
        this.buscarNodos = [];
        this.caminoNodo = '';
        this.sumarCamino = 0;
    }

    agregarNodoPadre() {
        var nodo = new Nodo(null, null, "21", 21);
        return nodo;
    }

    agregarNodo(nodoPadre, posicion, nombre, valor) {
        var nodo = new Nodo(nodoPadre, posicion, nombre, valor);
        return nodo;
    }

    verificarNivelHijos(nodo) { //Verifica los hijos en el nivel

        if (nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if (nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if (nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;

    }

    buscarValor(buscarElemento, nodo) { //busca el elemento

        if (nodo.valor == buscarElemento)
            this.busquedaElemento = nodo;

        if (nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento, nodo.hI);

        if (nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento, nodo.hD);

        return this.busquedaElemento;
    }

    buscarCaminoNodo(nodo) { //recorrido
        if (nodo.padre != null) {
            this.caminoNodo = this.caminoNodo + ',' + nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre)
        }
        return this.busquedaElemento.nombre + ' ' + this.caminoNodo;
    }

    sumarCaminoNodo(nodo) { //Suma de recorrido
        if (nodo.padre != null) {
            this.sumarCamino = this.sumarCamino + nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre)
        }
        return this.busquedaElemento.valor + this.sumarCamino;
    }
}